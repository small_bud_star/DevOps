#! /bin/bash
#==============================================================#
#   Description:  Unixbench script                             #
#   Author: Teddysun <i@teddysun.com>                          #
#   Intro:  https://teddysun.com/245.html                      #
#==============================================================#
cur_dir=/opt/download/fio

if [ ! -f ${cur_dir} ];then
    mkdir -p ${cur_dir}
fi

# Check System
[[ $EUID -ne 0 ]] && echo 'Error: This script must be run as root!' && exit 1
[[ -f /etc/redhat-release ]] && os='centos'
[[ ! -z "`egrep -i debian /etc/issue`" ]] && os='debian'
[[ ! -z "`egrep -i ubuntu /etc/issue`" ]] && os='ubuntu'
[[ "$os" == '' ]] && echo 'Error: Your system is not supported to run it!' && exit 1

# Install necessary libaries
if [ "$os" == 'centos' ]; then
    yum -y install make automake gcc autoconf gcc-c++ time perl-Time-HiRes libaio-devel
else
    apt-get -y update
    apt-get -y install make automake gcc autoconf time perl
fi

# Create new soft download dir
mkdir -p ${cur_dir}
cd ${cur_dir}

# Download UnixBench5.1.3
if [ -s fio-2.1.10.tar.gz ]; then
    echo "fio-2.1.10.tar.gz [found]"
else
    echo "fio-2.1.10.tar.gz not found!!!download now..."
    if ! wget -c https://dl.lamp.sh/files/fio-2.1.10.tar.gz; then
        echo "Failed to download fio-2.1.10.tar.gz, please download it to ${cur_dir} directory manually and try again."
        exit 1
    fi
fi
tar -zxvf fio-2.1.10.tar.gz 
cd fio-fio-2.1.10/

make
make install

echo
echo
echo "======= Script description and score comparison completed! ======= "
echo
echo
